For all the testcases, we use google chrome

A. Test for GET : get a response from server 
In this test, we open the man-page for recv, and below is part of the information printed in log file. From the information we can see method for this request is GET, we should check the cache to find the corresponding response. The response is not found, but could be cached. Proxy get response from server.
The request time is in UTC, and the expire time for response is set by us, because no s-maxage or max-age or expires appear in request header.

1: "GET http://man7.org/linux/man-pages/man2/recv.2.html HTTP/1.1" from 10.197.124.235 @ Sat Mar  3 01:44:56 2018

1: Requesting "GET http://man7.org/linux/man-pages/man2/recv.2.html HTTP/1.1 from man7.org
1: not in cache
1: Received "HTTP/1.1 200 OK" from man7.org
1: cached, expires at Thu Dec 31 23:59:59 2020

1: Responding "HTTP/1.1 200 OK

B. Test for GET : get a response from proxy cache
We open the man-page for recv again without shutting down our proxy, the information in log shows as below:

12: "GET http://man7.org/linux/man-pages/man2/recv.2.html HTTP/1.1" from 10.197.124.235 @ Sat Mar  3 01:45:13 2018

12: has age 16 while freshLifeTime is 89417702
12: in cache, valid.
12: Responding "HTTP/1.1 200 OK with originTime Sat Mar  3 01:44:57 2018

From these information, we can see that response for this response is found in cache, response age is 16 while its freshlifetime is 89417702, so this response is still valid, proxy send this response to client.

C. Test for GET : Expiretime
In testcase A, the request and response did not specify anything about s-maxage, max-age or expires, so we use a default expire time.
In this testcase, we can see that the response is cached, and expires at Sat Jul 26 06:00:00 1997. So we get a new response from server.
18: Requesting "GET http://c.statcounter.com/t.php?sc_project=7422636&java=1&security=9b6714ff&u1=47E48FD63BED4FE40909F5C99268ABEF&sc_random=0.0888456789385228&jg=new&rr=1.1.1.1.1.1.1.1.1&resolution=1920&h=1080&camefrom=&u=http%3A//man7.org/linux/man-pages/man2/recv.2.html&t=recv(2)%20-%20Linux%20manual%20page&rcat=d&rdom=d&rdomg=new&bb=1&sc_snum=1&sess=7a9eb4&p=0&invisible=1 HTTP/1.1 from c.statcounter.com
18: not in cache
18: Received "HTTP/1.1 200 OK" from c.statcounter.com
18: cached, expires at Sat Jul 26 06:00:00 1997

D. Test for CONNECT
In this test, we open the man-page for recv, and below is part of the information printed in log file.

11: "CONNECT tools.ietf.org:443 HTTP/1.1" from 10.197.124.235 @ Sat Mar  3 01:45:11 2018

11: Requesting "CONNECT tools.ietf.org:443 HTTP/1.1 from tools.ietf.org
11: Responsing "HTTP/1.1 200 OK"

From these information we can see our proxy get the CONNECT request from client and send it to the server, the server send back the corresponding response and proxy send it to client. The HTTP 200 OK success status response code indicates that the request has succeeded. 

E. Test for POST
3: "POST http://vcm-207.vm.duke.edu:8000/rsvp/create_choice/2/3/ HTTP/1.1" from 10.197.124.235 @ Sat Mar  3 03:07:20 2018

3: Requesting "POST http://vcm-207.vm.duke.edu:8000/rsvp/create_choice/2/3/ HTTP/1.1 from vcm-207.vm.duke.edu
3: Responding "HTTP/1.1 302 Found with originTime Sat Mar  3 03:07:20 2018
"
3: Received "HTTP/1.1 302 Found" from vcm-207.vm.duke.edu

In this test, we visited our RSVP website in hw1, created an event. This request is a POST request to create a choice.
We successfully created the choice and can find it in our database.

F. Other tests
Using our proxy, we can open website with large amount of request such as BBC.com.
We can open multiple web and visit the same website at the same time

G. Reference
https://beej.us/guide/bgnet/html/multi/getaddrinfoman.html
