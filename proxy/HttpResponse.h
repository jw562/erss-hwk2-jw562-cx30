#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>

#include <functional>
#include <string>
#include <cstddef>
#include <vector>

#include<ctime>
#include<time.h>
#include<limits.h>
#include<map>
//#include "HttpRequest.h"

using namespace std;
#define backlog 20
#define _BSD_SOURCE
#define buffsize 280000


class HttpResponse {
  
 private:

  //The entire response. 
  vector<char> response;
  vector<char> ETag;
  bool hasETag_;
  int size;
  
  //For example: HTTP/1.1 200 OK
  //Useful when receving response from server.
  string RESPONSE;

  //Origin time, expire time, and current age.
  struct tm originTime;
  struct tm expireTime;
  int age;

  //Must revalidate and manAge
  int noCache;
  int mustRevalidate;
  int freshLifeTime;

  //Dictionarys.
  map<string, int>days;
  map<string, int>months;
  
  //Initialize the days and months maps.
  void initDaysMonths() {
    days["Mon"] = 1;
    days["Tue"] = 2;
    days["Wed"] = 3;
    days["Thu"] = 4;
    days["Fri"] = 5;
    days["Sat"] = 6;
    days["Sun"] = 0;
    months["Jan"] = 0;
    months["Feb"] = 1;
    months["Mar"] = 2;
    months["Apr"] = 3;
    months["May"] = 4;
    months["Jun"] = 5;
    months["Jul"] = 6;
    months["Aug"] = 7;
    months["Sep"] = 8;
    months["Oct"] = 9;
    months["Nov"] = 10;
    months["Dec"] = 11;
  }

  int getDays(int day, int month) {
    map<int, int> mdays;
    mdays[0] = 31; mdays[1] = 28; mdays[2] = 31;
    mdays[3] = 30; mdays[4] = 31; mdays[5] = 30;
    mdays[6] = 31; mdays[7] = 31; mdays[8] = 30;
    mdays[9] = 31; mdays[10] = 30; mdays[11] = 30;
    int ans = 0;
    for (int i = 0; i < month; i++)ans = ans + mdays[i];
    ans = ans + day;
    return ans;
  }


  int getInt(string s) {
    int ans = 0;
    int i = 0;
    while(1) {
      if (i == s.length() || s[i] == '\0' || s[i] > '9' || s[i] < '0') break;
      ans = ans * 10 + (s[i] - '0');
      i++;
    }
    return ans;
  }

  void initETag(vector<char> res) {
    string temp(res.begin(), res.end());
    int start = temp.find("ETag:");
    int i = start+6;
    if (start != string::npos) {
      while(1) {
	if (res[i] == '\r') break;
	ETag.push_back(res[i]);
	i++;
      }
      hasETag_ = true;
    } else {
      hasETag_ = false;
    }
  }



  void initRESPONSE(vector<char> res) {
    string temp(res.begin(), res.end());
    RESPONSE = temp.substr(0, temp.find("\r\n"));
  }
  
    

  //Initialize the originTime field.
  void initOriginTime(vector<char> res) {
    string temp(res.begin(), res.end());
    if (temp.find("Date:") == string::npos) {
      time_t currTime = time(0);
      struct tm* nowTime = gmtime(&currTime);
      originTime = *nowTime;
    } else {
      int start = temp.find("Date:") + 6;
      temp = temp.substr(start, temp.length());
      temp = temp.substr(0, temp.find(" GMT"));
      originTime.tm_sec = getInt(temp.substr(23, 25));
      originTime.tm_min = getInt(temp.substr(20, 22));
      originTime.tm_hour = getInt(temp.substr(17, 19));
      originTime.tm_mday = getInt(temp.substr(5, 7));
      originTime.tm_mon = months[temp.substr(8, 3)];      
      originTime.tm_year = getInt(temp.substr(12, 16)) - 1900;
      originTime.tm_wday = days[temp.substr(0, 3)];
      //originTime.tm_yday = getDays(getInt(temp.substr(5, 7)), months[temp.substr(8, 11)]);
      originTime.tm_isdst = 0;
    }
  }

  
  //Initialzie the expireTime field.
  void initExpireTime(vector<char> res){
    string temp(res.begin(), res.end());
    if (temp.find("Cache-Control") != string::npos && temp.find("s-maxage=") != string::npos){
      string temp_substr = temp.substr(temp.find("s-maxage=")+9, temp.length());
      freshLifeTime = getInt(temp_substr);
      time_t t = mktime(&originTime);      
      t = t + freshLifeTime - 18000;
      struct tm* newExpireTime = gmtime(&t);
      expireTime = *newExpireTime;
    } else if (temp.find("Cache-Control") != string::npos && temp.find("max-age=") != string :: npos) {
      string temp_substr = temp.substr(temp.find("max-age=")+8, temp.length());
      freshLifeTime = getInt(temp_substr);
      time_t t = mktime(&originTime);
      t = t + freshLifeTime - 18000;
      struct tm* newExpireTime = gmtime(&t);
      expireTime = *newExpireTime;
    } else if (temp.find("Expires: ")!= string :: npos) {
      int start = temp.find("Expires: ") + 9;
      temp = temp.substr(start, temp.length());
      temp = temp.substr(0, temp.find(" GMT"));

      expireTime.tm_sec = getInt(temp.substr(23, 25));
      expireTime.tm_min = getInt(temp.substr(20, 22));
      expireTime.tm_hour = getInt(temp.substr(17, 19));
      expireTime.tm_mday = getInt(temp.substr(5, 7));
      expireTime.tm_mon = months[temp.substr(8, 3)];
      expireTime.tm_year = getInt(temp.substr(12, 16)) - 1900;
      expireTime.tm_wday = days[temp.substr(0, 3)];
      //expireTime.tm_yday = getDays(getInt(temp.substr(5, 7)), months[temp.substr(8, 11)]);
      expireTime.tm_isdst = 0;
      
      time_t beginning = mktime(&originTime);
      time_t end = mktime(&expireTime);
      freshLifeTime = (int)difftime(end, beginning);
    } else {
      //This means Expire time is not specified.
      expireTime.tm_sec = 59;
      expireTime.tm_min = 59;
      expireTime.tm_hour = 23;
      expireTime.tm_mday = 31;
      expireTime.tm_mon = 11;
      expireTime.tm_year = 2018 - 1898;
      expireTime.tm_wday = 6;
      expireTime.tm_yday = 365;
      expireTime.tm_isdst = 0;
      time_t beginning = mktime(&originTime);
      time_t end = mktime(&expireTime);
      freshLifeTime = (int)difftime(end, beginning);
      
    }
  }
      
  
 /////////////////////////////////////////////////////////////////////////////////////////////////////
 public:
  //Constructor
  HttpResponse(vector<char> res) {
    initDaysMonths();
    response = res; size = res.size();
    initRESPONSE(res);
    initOriginTime(res);
    initExpireTime(res);
    initETag(res);
    string temp(res.begin(), res.end());
    mustRevalidate = temp.find("must-revalidate") == string::npos ? 0 : 1;
    noCache = temp.find("no-cache") == string :: npos? 0 : 1;
  }

  //Return an array representation of the response.
  char* toArray() {
    char* ans = new char[response.size()];
    memset(ans, '\0', response.size());
    for (int i = 0; i < response.size(); i++) ans[i] = response[i];
    return ans;
  }


  //A series of get() functions.
  int getAge() {
    //Get and print current time. 
    time_t currTime = time(0);
    struct tm* nowTime = gmtime(&currTime);
    const char* t = asctime(nowTime);
    cout << "The current time UTC/GMT is: " << string(t) << endl;
    
    time_t bornTime = mktime(&originTime);
    int ans = currTime - bornTime + 18000;
    return ans;
  }


  string getOriginTime() {
    const char* t = asctime(&originTime);
    return string(t);
  }

  
  string getExpireTime() {
    const char* t = asctime(&expireTime);
    return string(t);
  }

  
  int getFreshLifeTime() {
    return freshLifeTime;
  }

  
  int getMustRevalidate() {
    return mustRevalidate;
  }

  
  int getNoCache() {
    return noCache;
  }

  
  vector<char> getETag() {
    return ETag;
  }

  string getETag_str() {
    return string(ETag.begin(), ETag.end());
  }

  bool isCacheable() {
    string temp(response.begin(), response.end());
    if (temp.find("200 OK") == string::npos || (temp.find("no-store") != string::npos)) return false;
    return true;
  }

  vector<char> getResponse() {
    return response;
  }

  vector<char> generateETag(string req) {
    string temp = "GET / HTTP/1.1\r\nIf-None-Match: ";
    string etag_str(ETag.begin(), ETag.end());
    temp = temp + etag_str + "\r\n\r\n";
    vector<char> ans;
    for (int i = 0; i < temp.size(); i++) ans.push_back(temp[i]);
    return ans;
  }

  bool hasETag() {
    return hasETag_;
  }

  string getRESPONSE() { return RESPONSE;}
  string getRESPONSEandTIME() {
    return (RESPONSE + " with originTime " + getOriginTime());
  }
  
};
