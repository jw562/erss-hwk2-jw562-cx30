#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>

#include <iostream>
#include <fstream>
#include <functional>
#include <string>
#include <map>
#include <cstddef>
#include <vector>
#include <unordered_map>
#include <exception>

#include "HttpRequest.h"
#include "HttpResponse.h"

using namespace std;
#define backlog 20
#define _BSD_SOURCE
#define buffsize 280000

unordered_map<string, HttpResponse> cache;
pthread_mutex_t myLock;
int req_uid;
ofstream logFile("/var/log/erss/proxy.log");


struct _myArgs {
  int threadNum;
  int sockfd;
  string clientIP;
  int UID;
};
typedef struct _myArgs myArgs;


char* toArr(vector<char> v) {
  char* ans = new char[v.size()]();
  memset(ans, '\0', v.size());
  for (int i = 0; i < v.size(); i++) ans[i] = v[i];
  return ans;
}


//TT
//Given a string that represents the web-address,
//return a string representing the host IP in NETWORK BYTE ORDER.
char* getIPAddress(const char* hostname) {
  struct hostent* h = gethostbyname(hostname);
  char* buff = new char[256]();
  if (h == NULL) { perror("Gethostbyname"); exit(1);};
  struct in_addr** addrs = (struct in_addr **)h->h_addr_list;
  for (int i = 0; addrs[i] != NULL; i++){
    strcpy(buff, inet_ntoa(*addrs[i]));
  }
  return buff;
}


string getServname(const char* req) {  
  string request(req);

  try {
    size_t pos = request.find("Host: ");
    pos = pos + 6;
    string request_substr = request.substr(pos, request.length());
    request_substr = request_substr.substr(0, request_substr.find("\r\n"));
  
    //Now the string request_substr only contains the domain name, and probably a port number.
    //For example: "duke.edu:433" or "www.google.com:433"
    size_t colonIndex = request_substr.find(":");
    if (colonIndex == string::npos){
      //The request did not provide a port number. Let's just use the default port number: 80.
      return string("80");
    } else {
      return request_substr.substr(colonIndex + 1, request_substr.length());
    }

  } catch(exception& e) {
    return "";
  }
}


string getNodename(const char* req) {
  string request(req);
  size_t pos = request.find("Host: ");
  if (pos == string::npos) {
    cout << "Host information not included in the header fields." << endl;
    return "";
  }
  pos = pos + 6;
  string request_substr = request.substr(pos, request.length());
  request_substr = request_substr.substr(0, request_substr.find("\r\n"));
  
  //Now the string request_substr only contains the domain name, and probably a port number.
  //For example: "duke.edu:433" or "www.google.com:433"
  size_t colonIndex = request_substr.find(":");
  if (colonIndex == string::npos){
    //The request did not provide a port number. Let's just use the default port number: 80.
    return request_substr;
  } else {
    return request_substr.substr(0, colonIndex);
  }
}


//Handle a CONNECT request after establishing a connection between the proxy and the origin server.
//req is the request, clientfd and serverfd refer to sockef fds of both sides.
int handleCONNECT(const char* req, int clientfd, int serverfd, void* args) {
  myArgs* m = (myArgs*)args;
  char buffer[buffsize];
  memset(buffer, '\0', buffsize);
  fd_set readfds;
  struct timeval tv; tv.tv_sec = 10;  tv.tv_usec = 1000000;
  FD_ZERO(&readfds); FD_SET(clientfd, &readfds); FD_SET(serverfd, &readfds);
  int numfds = clientfd > serverfd? (clientfd+1) : (serverfd+1);
  while(1) {
    int flag = select(numfds, &readfds, NULL, NULL, &tv);
    if (flag == 0) { perror("Select"); cout << "Select time out." << endl; return -1; }
    //If ready to read from client fd.
    if (FD_ISSET(clientfd, &readfds)) {
      memset(buffer, '\0', buffsize);
      int recv_flag = recv(clientfd, buffer, buffsize, 0);
      if (recv_flag < 0) { perror("Recv from client when handling CONNECT"); return -1; }
      if (recv_flag == 0) { cout << "Client closed the connection. Exit." << endl; return -1; }
      int send_flag = send(serverfd, buffer, recv_flag, 0);
      if (send_flag < 0) { perror("Send to the server when handling CONNECT"); return -1; }
      //cout << "Received " << recv_flag <<  " bytes from the client and sent " << send_flag << " bytes to the server" << endl;
    }
    //If ready to read from the server fd. 
    if (FD_ISSET(serverfd, &readfds)) {
      memset(buffer, '\0', buffsize);
      int recv_flag = recv(serverfd, buffer, buffsize, 0);
      if (recv_flag < 0) { perror("Recv from server when handling CONNECT"); return -2; }
      if (recv_flag == 0) { cout << "Server closed the connection. Exit." << endl; return -2; }
      int send_flag = send(clientfd, buffer, recv_flag, 0);
      if (send_flag < 0) {
	perror("Send to the client when handling CONNECT");
	return -2;
      }
      //cout << "Received " << recv_flag << " bytes from the server and sent " << send_flag << " bytes to the client" << endl;
    }
    FD_ZERO(&readfds); FD_SET(clientfd, &readfds); FD_SET(serverfd, &readfds);
  }
}


//This function takes a response and return the Content-Length field if the response as an int.
//If the response does not have this field, return -1. 
int getLength(const char* res){
  string response(res);
  int pos = response.find("Content-Length: ");
  if (pos == string::npos) { cout << "The response does not have a Content-Length field." << endl; return -1;}
  pos = pos + 16;
  int end = response.substr(pos, response.length()).find("\r\n");
  string temp = response.substr(pos, end);
  int ans = 0;
  for (int i = 0; i < temp.length(); i++) {
    ans = ans * 10 + (temp[i] - '0');
  }
  return ans;
}


//This function reads the response from a socket fd and return it as a vector<char> 
vector<char> readResponse(int sockfd, void* args) {
  myArgs* m = (myArgs*) args;
  //cout << "Thread " << m->threadNum << " started reading response from the server." << endl;
  char* buffer = new char[buffsize]();
  memset(buffer, '\0', buffsize);
  

  fd_set readfds;
  struct timeval tv; tv.tv_sec = 5;  tv.tv_usec = 250000;
  FD_ZERO(&readfds); FD_SET(sockfd, &readfds); 
  int numfds = sockfd + 1;
  int select_flag = select(numfds, &readfds, NULL, NULL, &tv);
  if (select_flag == 0) { cout << "Thread " << m->threadNum << " select time out when waiting for response from server." << endl; return vector<char>();} 
  
  int flag = recv(sockfd, buffer, buffsize, 0);
  
  if (flag < 0) {
    perror("Recv");
    return vector<char>();
  }
  if (flag == 0) { cout << "Thread " << m->threadNum;  perror("Recv returned 0 in the first call inside readResponses."); return vector<char>();}
  int length = getLength(buffer);
  
  if (length >= 0) {
    //Allocated a char[] dynamically that is able to store the entire response.
    //cout << "Entered the if statement in readResponse." << endl;
    string temp(buffer);
    int start = temp.find("\r\n\r\n") + 4;
    vector<char> ans;
    int numBytes = flag - start;
    //cout << "Thread " << m->threadNum << " Flag is: " << flag << " and start is: " << start << endl; 
    
    //Copy
    for (int i = 0; i < flag; i++) {ans.push_back(buffer[i]);}    
    
    while(numBytes != length) {
      //cout << "Entered while loop in readRequest." <<endl;
      memset(buffer, '\0', buffsize);
      int left = length - numBytes;
      //cout << "Number of bytes lefe is: " << left << endl;
      FD_ZERO(&readfds); FD_SET(sockfd, &readfds);
      select_flag = select(numfds, &readfds, NULL, NULL, &tv);
      if (select_flag == 0) { cout << "Thread " << m->threadNum << " select time out when waiting for response from server." << endl; return vector<char>();} 

      flag = recv(sockfd, buffer, buffsize, 0);
      //cout << "Thread " << m->threadNum << " recv returned: " << flag << endl;
      if (flag < 0) { perror("Recv"); return ans;}
      if (flag == 0) { perror("Recv returned 0 inside the while loop in readResponse."); return ans;}
      //Copy
      for (int i = 0; i < flag; i++) {
	ans.push_back(buffer[i]);
	numBytes++;
      }
    }
    
    delete[] buffer;
    return ans;
  } else {
    cout << "Thread " << m->threadNum << " mumber of bytes in the content of the response is negative!" << endl;
    vector<char> ans;
    for (int i = 0; i < flag; i++) {
      ans.push_back(buffer[i]);
    }
    return ans;
  }
}


//Handle an incomming request: GET, POST, or CONNECT.
int handleRequest(const char* req, int clientfd, void* args) {
  myArgs* m = (myArgs*)args;
  int sockfd;
  struct addrinfo hints, *servinfo, *p;
  int rv;

  //cout << "Thread " << m-> threadNum << " started handling request." << endl;
  //Set up a addrinfo struct and use it to connect to the origin server. 
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  string nodename_str = getNodename(req);
  const char* nodename = nodename_str.c_str();  //This is the domain name of the origin server. For example: "google.com"
  if (strlen(nodename) == 0) { close(clientfd); return -1;}
  const char* servname = getServname(req).c_str(); //This is the port number of the origin server in string form. For example: "80"  
  if ( (rv = getaddrinfo(nodename, servname, &hints, &servinfo)) != 0) { perror("Getaddrinfo"); return -1; }
  for (p = servinfo; p != NULL; p = p->ai_next) {
    if ( (sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1 ) { perror("Socket"); continue; }
    if ( connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) { perror("Connect"); close(sockfd); continue; }
    break;
  }
  if (p == NULL) {
    perror("Failed to connect");
    const char* badGateWay = "HTTP/1.1 502 Bad Gateway\r\n\r\n"; send(clientfd, badGateWay, strlen(badGateWay), 0);
    return -1;
  }
  struct sockaddr_in* s = (sockaddr_in*)p->ai_addr;
  //cout << "Thread " << m->threadNum << " connected successfully to: \"" << inet_ntoa(s->sin_addr) << "\""<< " on port: " << s->sin_port << endl;

  vector<char> v;
  for (int i = 0; i < strlen(req); i++) v.push_back(req[i]);
  HttpRequest req_obj(v);
  logFile << m->UID << ": Requesting \"" << req_obj.getREQUEST() << " from " << nodename << endl;
  //Check if the request is CONNECT. If so, handle it using the handleCONNECT function.
  string req_method(req);
  req_method = req_method.substr(0, req_method.find(" "));
  if (req_method.compare("CONNECT") == 0) {
    const char* res = "HTTP/1.1 200 OK\r\n\r\n";
    int send_flag = send(clientfd, res, strlen(res), 0);
    if (send_flag <= 0) { cout << "Thread " << m->threadNum << ": "; perror("Send 200 OK to client on CONNECT"); return -1;}
    cout << "Thread " << m->threadNum << " 200 OK Sent successfully to the client." << endl;
    logFile << m->UID << ": Responsing \"HTTP/1.1 200 OK\"" << endl;
    int handle_flag = handleCONNECT(req, clientfd, sockfd, args);
    if (handle_flag == -1) { close(clientfd); close(sockfd); logFile << m->UID << ": Tunnel closed" << endl; return -1;}
    if (handle_flag == -2) { close(sockfd); close(clientfd); logFile << m->UID << ": Tunnel closed" << endl; return -2;}
    logFile << m->UID << ": Tunnel closed" << endl;
    return 0;
  }

  //Now, the request must not be CONNECT. 
  //Send the request from the client to the origin server and print how many bytes are sents.
  int send_flag = send(sockfd, req, strlen(req), 0);
  if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum << " sending request to the origin server failed." << endl; return -1; }
  
  //Receive the message from the origin server, print the number of bytes received, and print the received response. 
  vector<char> temp = readResponse(sockfd, args);

  HttpResponse r(temp);
  logFile << m->UID << ": Responding \"" << r.getRESPONSEandTIME() << "\"" << endl;

  
  char response[temp.size()]; memset(response, '\0', temp.size());
  for (int i = 0; i < temp.size(); i++) response[i] = temp[i];
  if (temp.size() == 0) { cout << "ReadResponse failed." << endl; return -1;}
  logFile << m->UID << ": Received \"" << r.getRESPONSE() << "\" from " << nodename << endl;

  //Now send the message back to the client.
  send_flag = send(clientfd, response, temp.size(), 0);
  if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum  << " sending response back to the client failed." << endl; return -1; }
  return 0;
}


//This function tells if a given response is stale with respect
//to a given request. 
bool isStale(HttpRequest req, HttpResponse res, void* args) {
  myArgs* m = (myArgs*)args;
  int age = res.getAge();
  if (req.getMaxAge() != -1) {
    bool ans = req.getMaxAge() < age;
    //logFile << m->UID << ": has age " << age << " and req has max-age = " << req.getMaxAge() << endl;
    return ans;
  } else if (req.getMaxStale() != -1) {
    bool ans = req.getMaxStale() < age - res.getFreshLifeTime();
    //logFile << m->UID << ": has age " << age << " and req has max-stale = " << req.getMaxStale() << endl;
    return ans;
  } else if (req.getMinFresh() != -1) {
    bool ans = res.getFreshLifeTime() - age < req.getMinFresh();
    //logFile << m->UID << ": has age " << age << " and req has min-fresh = " << req.getMinFresh() << endl;
    return ans;
  } else {
    bool ans = age > res.getFreshLifeTime();
    //logFile << m->UID << ": has age " << age << " while freshLifeTime is " << res.getFreshLifeTime() << endl;
    return ans;
  }
  return false;
}


//Handle an incomming request: GET, POST, or CONNECT.
int handleGET(HttpRequest r, const char* req, int clientfd, void* args) {
  myArgs* m = (myArgs*)args;
  int sockfd;
  struct addrinfo hints, *servinfo, *p;
  int rv;

  //Write the request to log. 
  cout << "Thread " << m-> threadNum << " started handling GET request:\n" << endl;
  cout << req << endl;
  
  //Set up a addrinfo struct and use it to connect to the origin server. 
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  string nodename_str = getNodename(req);
  const char* nodename = nodename_str.c_str();  //This is the domain name of the origin server. For example: "google.com"
  if (strlen(nodename) == 0) { close(clientfd); return -1;}
  const char* servname = getServname(req).c_str(); //This is the port number of the origin server in string form. For example: "80"  
  if ( (rv = getaddrinfo(nodename, servname, &hints, &servinfo)) != 0) { perror("Getaddrinfo"); return -1; }
  for (p = servinfo; p != NULL; p = p->ai_next) {
    if ( (sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1 ) { perror("Socket"); continue; }
    if ( connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) { perror("Connect"); close(sockfd); continue; }
    break;
  }
  if (p == NULL) {
    perror("Failed to connect");
    const char* badGateWay = "HTTP/1.1 502 Bad Gateway\r\n\r\n"; send(clientfd, badGateWay, strlen(badGateWay), 0);
    return -1;
  }
  
  struct sockaddr_in* s = (sockaddr_in*)p->ai_addr;
  string REQUEST = r.getREQUEST();
  unordered_map<string, HttpResponse>::iterator it = cache.find(REQUEST);
  
  //Case 1: If the response is not in the cache.
  if (it == cache.end()) {
    logFile << m->UID << ": Requesting \"" << REQUEST << " from " << nodename << endl; 
    
    //Send the request from the client to the origin server and print how many bytes are sent.
    logFile << m->UID << ": " << "not in cache" << endl;
    int send_flag = send(sockfd, req, strlen(req), 0);
    if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum << " sending request to the origin server failed." << endl; return -1; }
  
    //Receive the message from the origin server, print the number of bytes received, and print the received response. 
    vector<char> temp = readResponse(sockfd, args);
    char response[temp.size()]; memset(response, '\0', temp.size());
    for (int i = 0; i < temp.size(); i++) response[i] = temp[i];
    if (temp.size() == 0) { cout << "ReadResponse failed." << endl; return -1;}
    
    HttpResponse res(temp);
    logFile << m->UID << ": Received \"" << res.getRESPONSE() << "\" from " << nodename << endl;
    
    if (r.getNoStore() == 0 && res.isCacheable()) {
      pthread_mutex_lock(&myLock);
      //This response can be cached.
      pair<string, HttpResponse> p(REQUEST, res);
      cache.insert(p);
      if (r.getNoCache() == 1 || res.getNoCache() == 1) logFile << m->UID << ": cached, but requires revalidation." << endl;
      else logFile << m->UID << ": cached, expires at " << res.getExpireTime() << endl;
      pthread_mutex_unlock(&myLock);
    } else {
      logFile << m->UID << ": not cacheable because the response or request contains the no-store header, or the response is not 200 OK." << endl;
    }
    //Now send the message back to the client.
    send_flag = send(clientfd, response, temp.size(), 0);
    if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum  << " sending response back to the client failed." << endl; return -1; }
    logFile << m->UID << ": Responding \"" << res.getRESPONSE() << endl;
    return 0;
    
    //Case 2: The response is in the cache. 
  } else {

    //Subcase 1: We must revalidate the response in the cache. 
    if ((r.getNoCache() == 1 || it->second.getNoCache() == 1) && (it->second.hasETag() == true)) {
      logFile << m->UID << ": in cache, but requires validation" << endl;
      pthread_mutex_lock(&myLock);
      vector<char> ETag_ = (it->second).generateETag(REQUEST);
      const char* ETag = string(ETag_.begin(), ETag_.end()).c_str();

      logFile << m->UID << ": Requesting revalidation from " << nodename << endl;
      int send_flag = send(sockfd, ETag, strlen(ETag), 0);
      if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum << " sending request to the origin server failed." << endl;
	pthread_mutex_unlock(&myLock);
	return -1;
      }
      
      vector<char> temp = readResponse(sockfd, args);
      char response[temp.size()]; memset(response, '\0', temp.size());
      for (int i = 0; i < temp.size(); i++) response[i] = temp[i];
      if (temp.size() == 0) { cout << "ReadResponse failed." << endl;
	pthread_mutex_unlock(&myLock);
	return -1;
      }

      //Get feedback from the server. If 200 OK, cache it.
      HttpResponse res(temp);
      if (r.getNoCache() == 0 && res.isCacheable()) {
        //This response can be cached.
	cache.erase(it);
        pair<string, HttpResponse> p(REQUEST, res);
        cache.insert(p);
      }
      pthread_mutex_unlock(&myLock);
      logFile << m->UID << ": Received \"" << res.getRESPONSE() << "\" from " << nodename << endl;

      //Then send the response back to the client. 
      send_flag = send(clientfd, response, temp.size(), 0);
      if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum  << " sending response back to the client failed." << endl; return -1; }
      logFile << m->UID << ": Responding \"" << res.getRESPONSEandTIME() << endl;

    //Sub-case 2: In cache and is stale: Get a new response and if 200 OK then cache it. 
    } else if (isStale(r, it->second, args)) {
      pthread_mutex_lock(&myLock);
      logFile << m->UID << ": in cache, but expired at " << (it->second).getExpireTime() << endl;
      cache.erase(it);
      int send_flag = send(sockfd, req, strlen(req), 0);
      if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum << " sending request to the origin server failed." << endl;
	pthread_mutex_unlock(&myLock);
	return -1;
      }
      logFile << m->UID << ": Requesting " << REQUEST << " from " << nodename;
  
      //Receive the message from the origin server, print the number of bytes received, and print the received response. 
      vector<char> temp = readResponse(sockfd, args);
      char response[temp.size()]; memset(response, '\0', temp.size());
      for (int i = 0; i < temp.size(); i++) response[i] = temp[i];
      if (temp.size() == 0) { cout << "ReadResponse failed." << endl;
	pthread_mutex_unlock(&myLock);
	return -1;
      }
      
      HttpResponse res(temp);
      logFile << m->UID << ": Received \"" << res.getRESPONSE() << "\" from " << nodename << endl;
      if (r.getNoCache() == 0 && res.isCacheable()) {
        //This response can be cached.
        pair<string, HttpResponse> p(REQUEST, res);
        cache.insert(p);
      }
      pthread_mutex_unlock(&myLock);
      //Now send the message back to the client.
      send_flag = send(clientfd, response, temp.size(), 0);
      if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum  << " sending response back to the client failed." << endl; return -1; }
      logFile << m->UID << ": Responding \"" << res.getRESPONSE() << endl;
      return 0;

    //Sub-case 3: In cache, valid. 
    } else {
      logFile << m->UID << ": in cache, valid." << endl;
      vector<char> temp = it->second.getResponse();
      const char* response = toArr(temp);
      int send_flag = send(clientfd, response, temp.size(), 0);
      if (send_flag <= 0) { perror("Send"); cout << "Thread " << m->threadNum  << " sending response back to the client failed." << endl; return -1; }
      logFile << m->UID << ": Responding \"" << it->second.getRESPONSEandTIME() << endl;
      return 0;
    }
    return 0;
  }
}


//Read a request when there is a Content-Length field in the request. 
vector<char> readPost(int sockfd, char* buffer, int recv_flag) {
  vector<char> ans;
  string temp(buffer);
  //Copy
  for (int i = 0; i < recv_flag; i++) ans.push_back(buffer[i]);
  int start = temp.find("\r\n\r\n") + 4;
  int length = getLength(buffer);
  int numBytes = recv_flag - start;
  while(numBytes != length) {
    memset(buffer, '\0', buffsize);
    int left = length - numBytes;
    recv_flag = recv(sockfd, buffer, buffsize, 0);
    if (recv_flag < 0) { perror("Recv"); return vector<char>();}
    if (recv_flag == 0) { perror("Recv returned 0 inside the while loop in readResponse."); return ans;}
    //Copy
    for (int i = 0; i < recv_flag; i++) {
      ans.push_back(buffer[i]);
      numBytes++;
    }
  }
  return ans;
}



//This function allows a thread to read request from a client
//and to return a vector. 
vector<char> readRequest(int sockfd, void* args) {
  myArgs* m = (myArgs*)args;
  char buffer[buffsize];
  memset(buffer, '\0', buffsize);

  fd_set readfds;
  struct timeval tv; tv.tv_sec = 2;  tv.tv_usec = 100000;
  FD_ZERO(&readfds); FD_SET(sockfd, &readfds); 
  int numfds = sockfd + 1;
  int select_flag = select(numfds, &readfds, NULL, NULL, &tv);
  if (select_flag == 0) { cout << "Thread " << m->threadNum << " select time out when waiting for request from client." << endl; return vector<char>();} 
  
  int recv_flag = recv(sockfd, buffer, buffsize, 0);
  if (recv_flag == 0) { cout << "recv returned 0" << endl; return vector<char>();}
  if (recv_flag < 0) { perror("Read Request"); return vector<char>();}
  
  vector<char> ans;
  string temp(buffer);
  if (temp.find("Content-Length:") != string::npos) { return readPost(sockfd, buffer, recv_flag); }
  int pos = temp.find("\r\n\r\n");
  for (int i = 0; i < recv_flag; i++) ans.push_back(buffer[i]);

  if (pos != string::npos) {
    return ans;
  } else {
    while(pos == string::npos) {
      //Copy the current bytes in the buffer into the vector.
      memset(buffer, '\0', buffsize);
      recv_flag = recv(sockfd, buffer, buffsize, 0);
      if (recv_flag == 0) { return ans;}
      if (recv_flag < 0) { perror("Read Request"); return vector<char>();}
      temp = string(buffer); pos = temp.find("\r\n\r\n");
      for (int i = 0; i < recv_flag; i++) ans.push_back(buffer[i]);
    }
  }
  //Return the answer.
  return ans;
}


//This function processes each accept.
//Intended for multi-thread processing. 
void* processAccept(void* args) {
  myArgs* m = (myArgs*)args;
  int newfd = m->sockfd;
  
  string request("");
  while(1) {
    vector<char> req = readRequest(newfd, args);
    const char* req_str = toArr(req);
    if (req.size() == 0) { //The client close the connection.
      close(newfd);
      cout << "The client close the connection or an error happened. Sockfd " << newfd << " closed." <<  endl;
      break;
    }
    HttpRequest r(req);
    pthread_mutex_lock(&myLock);
    req_uid = req_uid + 1;
    m->UID = req_uid;
    pthread_mutex_unlock(&myLock);
    logFile << m->UID << ": " << "\"" << r.getREQUEST() << "\"" << " from " << m->clientIP << " @ " << r.getTIME() << endl;

    //Check whether this request is in bad format (e.g. did not include a hostname information) If so, send a 404 Bad Request back.
    if (string(req.begin(), req.end()).find("Host:") == string::npos) {
      const char* badRequest = "HTTP/1.1 400 Bad Request\r\n\r\n";
      int send_flag = send(newfd, badRequest, strlen(badRequest), 0);
      return NULL;
    }
    
    //The request is well-formated. 
    if (!r.isGET()) {
      //The request is not GET.
      int handle_flag = handleRequest(req_str, newfd, args);
      if (handle_flag == -2 || handle_flag == -1) { break; }
    } else {
      //The request is GET 
      int handle_flag = handleGET(r, req_str, newfd, args);
      if (handle_flag == -2 || handle_flag == -1) { break; }
    }
    
  }
  return NULL;
}


void* doNothing(void* args) { return NULL; }


//The main function. 
//The main function. 
int main(int argc, char* argv[]){

  pthread_mutex_init(&myLock, NULL);
  req_uid = 0;
  logFile << "Started Recording GET Requests:" << endl;
  
  
  int sockfd, newfd;
  struct sockaddr_in server_addr, client_addr, remote_addr;
  //Set up a new socket file descriptor for listening incoming connections.
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("Socket");
    exit(1);
  }

  //Bind the socket to a specific port chosen by the kernel.
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(8080);
  server_addr.sin_addr.s_addr = INADDR_ANY;
  memset(&(server_addr.sin_zero), '\0', 8);
  if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(struct sockaddr)) < 0){
    perror("Binding");
    exit(1);
  }
  
  //cout << "Successfully binded socket " << sockfd << " to port "<< server_addr.sin_port << " on address " << server_addr.sin_addr.s_addr << endl;
  //Listen for incomming connections. If the there is an incomming connection request, accept it. 
  if (listen(sockfd, backlog) < 0) { perror("Listen"); exit(1);}
  socklen_t client_length = (socklen_t) sizeof(struct sockaddr);


  //The multithread version.
  int nThreads = 50;
  
  while(1) {
    pthread_t* threads = (pthread_t*)malloc(nThreads*sizeof(*threads));
    //Once accept()'ed, create a new thread to handle that.
    myArgs args[nThreads];
    
    for (int i = 0; i < nThreads; i++) {
      //cout << "Thread " << i << " is waiting for new connection." << endl;
      newfd = accept(sockfd, (struct sockaddr*)&client_addr, &client_length);
      if (newfd < 0) {
	perror("Accept");
	close(newfd);
	int p = pthread_create(&threads[i], NULL, doNothing, NULL);
	if (p != 0) continue;
      }
      //myArgs* m = (myArgs*)malloc(sizeof(*m));
      args[i].threadNum = i;
      args[i].sockfd = newfd;
      args[i].clientIP = string(inet_ntoa(client_addr.sin_addr)); //This is not important for now.
      args[i].UID = -1;
      //cout << "Accepted and all fields of myArgs allocated: " << i <<  endl;
      int p = pthread_create(&threads[i], NULL, processAccept, &args[i]);
      if (p != 0) {
	perror("pthread_create");
	close(newfd);
	continue;
      }
    }
    
    //Wait for all the threads to complete.
    for (int i = 0; i < nThreads; i++) {
      pthread_join(threads[i], NULL);
      cout << "Thread " << i << " is finished." << endl;
    }
    cout << "All threads are done processing accepts." << endl;
    free(threads);
  }
  return 0;  
} 
