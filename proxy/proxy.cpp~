#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>

#include <string>
#include <cstddef>

using namespace std;
#define backlog 20
#define _BSD_SOURCE
#define buffsize 280000



//TT
//Given a string that represents the web-address,
//return a string representing the host IP in NETWORK BYTE ORDER.
char* getIPAddress(const char* hostname) {
  struct hostent* h = gethostbyname(hostname);
  char* buff = new char[256]();
  if (h == NULL) { perror("Gethostbyname"); exit(1);};
  struct in_addr** addrs = (struct in_addr **)h->h_addr_list;
  for (int i = 0; addrs[i] != NULL; i++){
    strcpy(buff, inet_ntoa(*addrs[i]));
  }
  return buff;
}



string getServname(const char* req) {  
  string request(req);
  size_t pos = request.find("Host: ");
  if (pos == string::npos) {
    cout << "Host information not included in the header fields." << cout;
    exit(1);
  }
  pos = pos + 6;
  string request_substr = request.substr(pos, request.length());
  request_substr = request_substr.substr(0, request_substr.find("\r\n"));
  
  //Now the string request_substr only contains the domain name, and probably a port number.
  //For example: "duke.edu:433" or "www.google.com:433"
  size_t colonIndex = request_substr.find(":");
  if (colonIndex == string::npos){
    //The request did not provide a port number. Let's just use the default port number: 80.
    return string("80");
  } else {
    return request_substr.substr(colonIndex + 1, request_substr.length());
  }
}


string getNodename(const char* req) {
  string request(req);
  size_t pos = request.find("Host: ");
  if (pos == string::npos) {
    cout << "Host information not included in the header fields." << cout;
    exit(1);
  }
  pos = pos + 6;
  string request_substr = request.substr(pos, request.length());
  request_substr = request_substr.substr(0, request_substr.find("\r\n"));
  
  //Now the string request_substr only contains the domain name, and probably a port number.
  //For example: "duke.edu:433" or "www.google.com:433"
  size_t colonIndex = request_substr.find(":");
  if (colonIndex == string::npos){
    //The request did not provide a port number. Let's just use the default port number: 80.
    return request_substr;
  } else {
    return request_substr.substr(0, colonIndex);
  }
}


//Handle a CONNECT request after establishing a connection between the proxy and the origin server.
//req is the request, clientfd and serverfd refer to sockef fds of both sides.
int handleCONNECT(const char* req, int clientfd, int serverfd) {
  char buffer[buffsize];
  memset(buffer, '\0', buffsize);
  fd_set readfds;
  struct timeval tv; tv.tv_sec = 2;  tv.tv_usec = 10000;
  FD_ZERO(&readfds); FD_SET(clientfd, &readfds); FD_SET(serverfd, &readfds);
  int numfds = clientfd > serverfd? (clientfd+1) : (serverfd+1);
  while(1) {
    int flag = select(numfds, &readfds, NULL, NULL, &tv);
    if (flag == -1) { perror("Select"); cout << "Select time out." << endl; close(clientfd); close(serverfd); return -1; }
    //If ready to read from client fd.
    if (FD_ISSET(clientfd, &readfds)) {
      memset(buffer, '\0', buffsize);
      int recv_flag = recv(clientfd, buffer, buffsize, 0);
      if (recv_flag < 0) { perror("Recv from client when handling CONNECT"); close(clientfd); close(serverfd); return -1; }
      if (recv_flag == 0) { cout << "Client closed the connection. Exit." << endl; close(clientfd); close(serverfd); return 0; }
      int send_flag = send(serverfd, buffer, recv_flag, 0);
      if (send_flag < 0) { perror("Send to the server when handling CONNECT"); close(clientfd); close(serverfd); return -1; }
      cout << "Received " << recv_flag <<  " bytes from the client and sent " << send_flag << " bytes to the server" << endl;
    }
    //If ready to read from the server fd. 
    if (FD_ISSET(serverfd, &readfds)) {
      memset(buffer, '\0', buffsize);
      int recv_flag = recv(serverfd, buffer, buffsize, 0);
      if (recv_flag < 0) { perror("Recv from server when handling CONNECT"); close(serverfd); close(clientfd); return -1; }
      if (recv_flag == 0) { cout << "Server closed the connection. Exit." << endl; close(serverfd); close(clientfd); return 0; }
      int send_flag = send(clientfd, buffer, recv_flag, 0);
      if (send_flag < 0) { perror("Send to the client when handling CONNECT"); close(serverfd); close(clientfd); return -1; }
      cout << "Received " << recv_flag << " bytes from the server and sent " << send_flag << " bytes to the client" << endl;
    }
    FD_ZERO(&readfds); FD_SET(clientfd, &readfds); FD_SET(serverfd, &readfds);
  }
}


//This function takes a response and return the Content-Length field if the response as an int.
//If the response does not have this field, return -1. 
int getLength(const char* res){
  string response(res);
  int pos = response.find("Content-Length: ");
  if (pos == string::npos) { cout << "The response does not have a Content-Length field." << endl; return -1;}
  pos = pos + 16;
  int end = response.substr(pos, response.length()).find("\r\n");
  string temp = response.substr(pos, end);
  int ans = 0;
  for (int i = 0; i < temp.length(); i++) {
    ans = ans * 10 + (temp[i] - '0');
  }
  return ans;
}


//This function reads the response from a socket fd and return it as a C++ string.
string readResponse(int sockfd) {
  char buffer[buffsize]; memset(buffer, '\0', buffsize);
  int flag = recv(sockfd, buffer, buffsize, 0);
  if (flag < 0) { perror("Recv"); return "";};
  int length = getLength(buffer);
  if (length > 0) {
    string temp(buffer);
    int start = temp.find("\r\n\r\n") + 4;
    int numBytes = strlen(buffer) - start;
    while(numBytes < length) {
      memset(buffer, '\0', buffsize);
      flag = recv(sockfd, buffer, buffsize, 0);
      if (flag < 0) { perror("Recv"); return "";};
      temp = temp + string(buffer);
      numBytes = numBytes + strlen(buffer);
    }
    return temp;
  } else {
    return string(buffer);
  }
}


//Handle an incomming request: GET, POST, or CONNECT.
int handleRequest(const char* req, int clientfd) {
  int sockfd;
  struct addrinfo hints, *servinfo, *p;
  int rv;

  //Set up a addrinfo struct and use it to connect to the origin server. 
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = 0;
  string nodename_str = getNodename(req);
  const char* nodename = nodename_str.c_str();  //This is the domain name of the origin server. For example: "google.com"    
  const char* servname = getServname(req).c_str(); //This is the port number of the origin server in string form. For example: "80"  
  if ( (rv = getaddrinfo(nodename, servname, &hints, &servinfo)) != 0) { perror("Getaddrinfo"); return -1; }
  for (p = servinfo; p != NULL; p = p->ai_next) {
    if ( (sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1 ) { perror("Socket"); continue; }
    if ( connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) { perror("Connect"); close(sockfd); continue; }
    break;
  }
  if (p == NULL) { perror("Failed to connect"); return -1;}
  struct sockaddr_in* s = (sockaddr_in*)p->ai_addr;
  cout << "Connected successfully to: \"" << inet_ntoa(s->sin_addr) << "\""<< " on port: " << s->sin_port << endl;
  
  //Check if the request is CONNECT. If so, handle it using the handleCONNECT function.
  string req_method(req);
  req_method = req_method.substr(0, req_method.find(" "));
  if (req_method.compare("CONNECT") == 0) {
    const char* res = "HTTP/1.1 200 OK\r\n\r\n";
    int send_flag = send(clientfd, res, strlen(res), 0);
    if (send_flag <= 0) { perror("Send 200 OK to client on CONNECT"); return -1;}
    cout << "200 OK Sent successfully to the client." << endl;
    handleCONNECT(req, clientfd, sockfd);
    
    return 0;
  }

  //Now, the request must not be CONNECT. 
  //Send the request from the client to the origin server and print how many bytes are sents.
  int send_flag = send(sockfd, req, strlen(req), 0);
  if (send_flag <= 0) { perror("Send"); cout << "Sending request to the origin server failed." << endl; return -1; }
  cout << "Message sent to the destination server successfully;" <<"strlen(req) is: " << strlen(req) << " and send_flag is: " << send_flag  << endl;
  
  //Receive the message from the origin server, print the number of bytes received, and print the received response. 
  string response = readResponse(sockfd);
  if (response.compare("") == 0) { cout << "ReadResponse failed." << endl; return -1;}
  const char* buffer = response.c_str();
  cout << "The response with length " << strlen(buffer) <<" that we received from the client is:" << endl; cout << buffer << endl; 

  //Now send the message back to the client.
  send_flag = send(clientfd, buffer, strlen(buffer), 0);
  if (send_flag <= 0) { perror("Send"); cout << "Sending response back to the client failed." << endl; return -1; }
  cout << "Response sent back to the client; " << "send_flag = " << send_flag << " and strlen(buffer) = " << strlen(buffer) << endl;
  return 0;
}


//The main function. 
//The main function. 
int main(int argc, char* argv[]){
    
  int sockfd, newfd;
  struct sockaddr_in server_addr, client_addr, remote_addr;
  //Set up a new socket file descriptor for listening incoming connections.
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("Socket");
    exit(1);
  }

  //Bind the socket to a specific port chosen by the kernel.
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(8080);
  server_addr.sin_addr.s_addr = INADDR_ANY;
  memset(&(server_addr.sin_zero), '\0', 8);
  if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(struct sockaddr)) < 0){
    perror("Binding");
    exit(1);
  }
  //cout << "Successfully binded socket " << sockfd << " to port "<< server_addr.sin_port << " on address " << server_addr.sin_addr.s_addr << endl;

  //Listen for incomming connections. If the there is an incomming connection request, accept it. 
  if (listen(sockfd, backlog) < 0) { perror("Listen"); exit(1);}
  socklen_t client_length = (socklen_t) sizeof(struct sockaddr);
  newfd = accept(sockfd, (struct sockaddr*)&client_addr, &client_length);
  if (newfd < 0) { perror("Accept"); exit(1); }
  
  //Receive the request message.
  while(1) {
    //if (listen(sockfd, backlog) < 0) { perror("Listen"); exit(1);}
    //socklen_t client_length = (socklen_t) sizeof(struct sockaddr);
    //newfd = accept(sockfd, (struct sockaddr*)&client_addr, &client_length);
    //if (newfd < 0) { perror("Accept"); exit(1); }
    char req[buffsize];
    memset(req, '\0', buffsize);
    int recv_flag = recv(newfd, req, buffsize, 0);
    if (recv_flag < 0) { perror("Recv for requests from client"); exit(1);}
    if (recv_flag == 0) {
      cout << "The client closed the connection. Let's reconnect." << endl;
      continue;
    }
    cout << "Here is the request just received with length " << recv_flag << ": " << endl;
    cout << req << endl;

    //Now get connected to the destination server and send the request.
    int handle_flag = handleRequest(req, newfd);
    if (handle_flag < 0) {cout << "Error occured when handling request." << endl; close(sockfd); close(newfd); exit(1); }
    //close(newfd);
  }
  return 0;
} 
